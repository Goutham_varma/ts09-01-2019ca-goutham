/*
 * Takes strings as input and prints them back in the order of their lengths.
 */

package printInOrderOfLength;
import java.util.*;
public class PrintStringsInOrderOfLength {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("How many strings do you want to enter? ");
		int string_count = userInput.nextInt(); //Number of strings the user want to enter
		userInput.nextLine();
		String[] array_of_strings = new String[string_count]; //array to store the strings
		System.out.println("Enter the strings: ");
		for(int i = 0;i<string_count;i++) {
			array_of_strings[i] = userInput.nextLine(); //each string input is added to the array
		}
		userInput.close();
		/*
		 * Bubble sorting logic. Keep updating the final element in the unsorted part by the 
		 * maximum value.
		 */
		for(int i = string_count - 1; i >=1;i--) {
			String max_length_string = array_of_strings[0]; //variable to store the string with maximum length
			int max_length = array_of_strings[0].length(); //variable to store the maximum length value
			int current_index = 0; //variable to store the index of the maximum length string
			for(int j = 0;j<=i;j++) {
				if(array_of_strings[j].length()>max_length) {
					max_length = array_of_strings[j].length(); //update the max length if the string length is larger than previous max length string
					max_length_string = array_of_strings[j]; //update the max length string
					current_index = j;	//update the index			 
				}
			}
			String temp_var = array_of_strings[i]; //swapping positions to send the max length string to the end of unsorted part
			array_of_strings[i] = max_length_string;
			array_of_strings[current_index] = temp_var;
		}
		System.out.println("The strings in the increasing order of their lengths are: ");
		for(int i = 0;i<array_of_strings.length;i++) { //printing the array
			System.out.println(array_of_strings[i]);
		}
	}

}
