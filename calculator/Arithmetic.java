/*
 * All the methods that do the arithmetic operations are defined in this class
 *
 * Addition, Subtraction, Multiplication, Divison, Square root, Exponentiation, Factorial
 * methods are developed
 *
 */

package calculator;

public class Arithmetic {
	//Addition Method
	double addition(double firstNum, double secondNum) {
		return firstNum + secondNum;
	}
	//Subtraction method
	double subtraction(double firstNum, double secondNum) {
		return firstNum - secondNum;
	}
	//Multiplication method
	double multiplication(double firstNum, double secondNum) {
		return firstNum * secondNum;
	}
	//Divison method
	double divison(double firstNum, double secondNum) {
		return firstNum / secondNum;
	}
	//Squareroot method
	double squareRoot(double number) {
		double sqrt = number/2; //sqrt is initialized with half the value of number variable
		double temp_var;
		do {
			temp_var = sqrt;
			sqrt = (temp_var + (number / temp_var)) / 2;
		}
		while((temp_var - sqrt) != 0);
		return sqrt;
	}
	//Exponentiation method
	double power(double firstNum, double secondNum) {
		double result = 1;
		while(secondNum >= 1) {
			result = result * firstNum;
			secondNum = secondNum - 1;
		}
		return result;
	}
	//Factorial method
	double factorial(double number) {
		double result = 1;
		for(double i = number; i >= 1; i--) {
			result = result * i;
		}
		return result;
	}
}
