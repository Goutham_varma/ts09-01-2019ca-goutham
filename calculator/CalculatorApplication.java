/*
 * This is the application that is run.
 */
package calculator;
import java.util.*;
public class CalculatorApplication extends Arithmetic{
	static Scanner userInput = new Scanner(System.in);
	static double firstNum, secondNum; //variables that will be used to store user input
	static double number;
	static int count = 0; //the variable that decides the run time of application.
	void Initialize() {
		if(count == 0) { //0 means it is run for the first time
			//menu that is printed for the user
			System.out.println("Hi there, what do you want me to do?");
			System.out.println("-------------------------------------------");
			System.out.println("Calculator Menu");
			System.out.println("-------------------------------------------");
			System.out.println("Enter + for Addition");
			System.out.println("Enter - for Subtraction");
			System.out.println("Enter * for Multiplicaton");
			System.out.println("Enter / for Divison");
			System.out.println("Enter r for SquareRoot");
			System.out.println("Enter e for exponentiation");
			System.out.println("Enter f for factorial");
			System.out.println("Enter 0 to exit");
			System.out.println("Enter c to clear screen and start again");
			System.out.println("------------------------------------------");
		}
		else {
			//when the user wants to continue with the same calculation. count is not 0
			System.out.println("Do you want me to do more?");
		}	
	}
	void initialize_two_values(){ //method that initializes the two input values. Part of refactoring
		System.out.println("Enter two numbers: ");
		firstNum = userInput.nextDouble(); 
		secondNum = userInput.nextDouble();
	}
	void initialize_one_value() { //method that initialized the one input value. part of refactoring.
		System.out.println("Enter a number: ");
		number = userInput.nextDouble();
	}

	public static void main(String[] args) {
		CalculatorApplication app = new CalculatorApplication(); //app is the calculator application object
		Arithmetic math = new CalculatorApplication(); //math is an object of type arithmetic
		double output = 0; //output. Initial value is 0
		try {
			//until the user enters the exit command this while loop will keep running.
			//To give the user the flexibility of doing calculations using any number of numbers.
			while(count>=0) { // to keep the application running as long as count is 0. That is user is doing a calculation
				app.Initialize(); //Initialized to display the menu to user.
				String user_preference = userInput.next(); //user preference is collected.
				char user_input = user_preference.charAt(0);
				if(user_input == '0') { //exit function
					System.out.println("The program is exiting.");
					System.out.println("The final result is "+output);
					output = 0;
					break; //here the loop breaks. application is stopped.
				}
				else if(user_input == '+') { //addition functionality
					if(output == 0) {
						app.initialize_two_values();
						output = math.addition(firstNum, secondNum);
					}
					else {
						app.initialize_one_value();
						output = math.addition(output, number);
					}
				}
				else if(user_input == '-') { //subtraction functionality
					if(output == 0) {
						app.initialize_two_values();
						output = math.subtraction(firstNum, secondNum);
					}
					else {
						app.initialize_one_value();
						output = math.subtraction(output, number);
					}
				}
				else if(user_input == '*') { //multiplication functionality
					if(output == 0) {
						app.initialize_two_values();
						output = math.multiplication(firstNum, secondNum);
					}
					else {
						app.initialize_one_value();
						output = math.multiplication(output, number);
					}
				}
				else if(user_input == '/') { //division functionality
					if(output == 0) {
						app.initialize_two_values();
						output = math.divison(firstNum, secondNum);
					}
					else {
						app.initialize_one_value();
						output = math.divison(output, number);
					}
				}
				else if(user_input == 'r') { //square root functionality
					if(output == 0) {
						app.initialize_one_value();
						output = math.squareRoot(number);
					}
					else {
						output = math.squareRoot(output);
					}
				}
				else if(user_input == 'e') { //exponentiation functionality
					if(output == 0) {
						app.initialize_two_values();
						output = math.power(firstNum, secondNum);
					}
					else {
						app.initialize_one_value();
						output = math.power(output, number);
					}
				}
				else if(user_input == 'f') { //factorial functionality
					if(output == 0) {
						app.initialize_one_value();
						output = math.factorial(number);
					}
					else {
						output = math.factorial(output);
					}
				}
				else if(user_input == 'c') { //clear the calculation and start a new calculation
					output = 0;
					count = 0;
					continue; //stops with the previous calculation and starts a new one.
				}
				else {
					throw new Exception(); //an exception is thrown if any other command apart from menu is given
				}
				count = count + 1;
				System.out.println("The result is "+output);
			}	
			
		}
		catch(Exception e) {
			System.out.println("There was an error. Try again"); //prints when there is an exception
		}
		finally {
			userInput.close();
		}
	}
}
