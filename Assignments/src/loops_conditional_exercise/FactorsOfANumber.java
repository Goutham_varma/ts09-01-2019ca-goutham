package loops_conditional_exercise;
import java.util.*;
public class FactorsOfANumber {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a number: ");
		long number = userInput.nextLong();
		userInput.close();
		System.out.print("Factors of "+number+" are ");
		for(int i=1;i<=number;i++) {
			if(number%i==0) {
				System.out.print(i+", ");
			}
		}
	}
}
