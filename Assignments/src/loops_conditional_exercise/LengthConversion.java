//program to convert length in centimeters to meters and kilometers
package loops_conditional_exercise;
import java.util.*;
public class LengthConversion {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter the length in centimeters: ");
		double cm = userInput.nextDouble();
		userInput.close();
		System.out.println("The length in meters is "+((1.0/100.0)*cm));
		System.out.println("The length in meters is "+((1.0/100000.0)*cm));		
	}
}
