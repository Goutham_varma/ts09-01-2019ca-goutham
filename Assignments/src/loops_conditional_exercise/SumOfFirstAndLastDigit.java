package loops_conditional_exercise;
import java.util.*;
public class SumOfFirstAndLastDigit {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a number: ");
		long num = userInput.nextLong();
		userInput.close();
		long lastDigit = num % 10;
		while((num/10)!=0) {
			num = num / 10;
		}
		long firstDigit = num;
		System.out.println("The sum of first and last digit in number is "+(firstDigit+lastDigit));
	}

}
