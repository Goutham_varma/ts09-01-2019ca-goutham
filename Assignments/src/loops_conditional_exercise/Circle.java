//program to calculate diameter, circumference and area of circle
package loops_conditional_exercise;
import java.util.*;
public class Circle {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter the radius of circle: ");
		double radius = userInput.nextDouble();
		userInput.close();
		System.out.println("The diameter of the circle is: "+2*radius);
		System.out.println("The area of the circle is "+(3.14 * radius * radius));
		System.out.println("The circumference of the circle is "+(2 * 3.14 * radius));

	}

}
