package loops_conditional_exercise;

import java.util.Scanner;

public class SumOfEvenNumbers {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter the number of natural numbers: ");
		int num = userInput.nextInt();
		userInput.close();
		int sum = 0;
		for(int i=1;i<=num;i++) {
			if(i%2 == 0) {
				sum = sum + i;
			}
		}
		System.out.println("The sum of even numbers in first "+num+" Natural numbers is "+sum);

	}

}
