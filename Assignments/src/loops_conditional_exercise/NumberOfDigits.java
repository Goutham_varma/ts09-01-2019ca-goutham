package loops_conditional_exercise;
import java.util.*;
public class NumberOfDigits {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a number: ");
		long num = userInput.nextLong();
		userInput.close();
		long temp = num/10;
		int count = 1;
		while(temp!=0) {
			count++;
			temp = temp/10;			
		}
		System.out.println("The number of digits in the number are "+count);
	}
}
