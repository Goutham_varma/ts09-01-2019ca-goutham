package loops_conditional_exercise;
import java.util.*;
public class OnesComlement {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a binary number: ");
		String binaryNumber = userInput.nextLine();
		userInput.close();
		StringBuffer output = new StringBuffer();
		for(int i = 0;i<binaryNumber.length();i++) {
			if (binaryNumber.charAt(i)=='1') {
				output.append('0');
			}
			else if(binaryNumber.charAt(i)=='0'){
				output.append('1');
			}
		}
		System.out.println("The One's complement of "+binaryNumber+" is "+output);
	}

}
