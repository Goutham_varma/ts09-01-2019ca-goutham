package loops_conditional_exercise;
import java.util.*;
public class LeapYear {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a year: ");
		int year = userInput.nextInt();
		userInput.close();
		System.out.println(((year%400==0)||((year%4==0)&&(year%100!=0)))?"Leap Year":"Not a leap year");
	}
}
