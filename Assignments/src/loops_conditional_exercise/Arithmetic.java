//Program to do basic arithmetic operations
package loops_conditional_exercise;
import java.util.*;
public class Arithmetic {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a number: ");
		double firstNum = userInput.nextDouble();
		System.out.println("Enter another number: ");
		double secondNum = userInput.nextDouble();
		userInput.close();
		System.out.println("Sum of two numbers is "+(firstNum+secondNum));
		System.out.println("Result after subtraction is "+(firstNum-secondNum));
		System.out.println("Product of two numbers is "+(firstNum*secondNum));
		System.out.println("Result after divison is "+(firstNum/secondNum));
	}
}
