package loops_conditional_exercise;
import java.util.*;
public class EvenOrOddUsingCO {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a number: ");
		int number = userInput.nextInt();
		userInput.close();
		System.out.println((number%2==0)?"Even":"Odd");
	}

}
