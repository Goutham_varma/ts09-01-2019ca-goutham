package loops_conditional_exercise;
import java.util.*;
public class HCFofNumber {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a number: ");
		int firstNum = userInput.nextInt();
		System.out.println("Enter another number: ");
		int secondNum = userInput.nextInt();
		userInput.close();
		int temp;
		if(firstNum>=secondNum) {
			while((firstNum%secondNum)!=0) {
				temp = secondNum;
				secondNum = firstNum % secondNum;
				firstNum = temp;
			}
			System.out.println("The GCD or HCF of given two numbers is: "+secondNum);
		}
		else {
			while((secondNum%firstNum)!=0) {
				temp = firstNum;
				firstNum = secondNum % firstNum;
				secondNum = temp;
			}
			System.out.println("The GCD or HCF of given two numbers is: "+firstNum);
		}
	}

}
