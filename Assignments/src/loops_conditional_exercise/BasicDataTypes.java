//Basic program to input and output primitive data types
package loops_conditional_exercise;
import java.util.*;
public class BasicDataTypes {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a byte number: ");
		byte bNum = userInput.nextByte();
		System.out.println("Enter an integer: ");
		int iNum = userInput.nextInt();
		System.out.println("Enter a float number: ");
		float fNum = userInput.nextFloat();
		System.out.println("Enter a double number: ");
		double dNum = userInput.nextDouble();
		System.out.println("Enter a character: ");
		char cChar = userInput.next().charAt(0);
		System.out.println("Enter a short number: ");
		short sNum = userInput.nextShort();
		System.out.println("Enter a long number: ");
		long lNum = userInput.nextLong();
		System.out.println("Enter a boolean value: ");
		boolean b = userInput.hasNext();
		userInput.close();
		System.out.println("Entered numbers are: ");
		System.out.println("Byte: "+bNum);
		System.out.println("int: "+iNum);
		System.out.println("short: "+sNum);
		System.out.println("long: "+lNum);
		System.out.println("double: "+dNum);
		System.out.println("float: "+fNum);
		System.out.println("char: "+cChar);
		System.out.println("Boolean: "+b);
	}

}
