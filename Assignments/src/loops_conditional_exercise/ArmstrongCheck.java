//checks if a number is armstrong number or not
package loops_conditional_exercise;
import java.util.*;
import java.lang.Math;
public class ArmstrongCheck {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a number: ");
		int number = userInput.nextInt();
		int num = number;
		userInput.close();
		int sum = 0;
		int digit;
		while((num/10)!=0) {
			digit = num%10;
			sum = sum + (int)Math.pow(digit, 3);
			num = num/10;
		}
		sum = sum + (int)Math.pow(num, 3);
		if(number == sum) {
			System.out.println("Yes, it is an armstrong number");
		}
		else {
			System.out.println("No it is not an armstrong number");
		}
	}

}
