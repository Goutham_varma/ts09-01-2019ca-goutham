//Program to find area of rectangle
package loops_conditional_exercise;
import java.util.*;
public class AreaOfRectangle {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter the length of rectangle: ");
		double length = userInput.nextDouble();
		System.out.println("Enter the breadth of rectangle: ");
		double breadth = userInput.nextDouble();
		userInput.close();
		System.out.println("The area of the rectangle is "+(length*breadth));

	}

}
