package loops_conditional_exercise;
import java.util.*;
public class SumOfDigits {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a number: ");
		long number = userInput.nextLong();
		userInput.close();
		long sum = 0;
		while(number!=0) {
			sum = sum + (number%10);
			number = number/10;
		}
		System.out.println("The sum of the digits in the number is "+sum);
	}

}
