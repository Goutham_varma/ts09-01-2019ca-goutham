//takes the length of side as input and prints the area of the equialteral triangle
package loops_conditional_exercise;
import java.util.*;
import java.lang.Math;
public class AreaOfEquilateralTriangle {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter the length of equilateral triangle: ");
		double length = userInput.nextDouble();
		userInput.close();
		double area = (Math.sqrt(3)/4.0)*length*length;
		System.out.println("Area of the equilateral triangle is "+area);
	}

}
