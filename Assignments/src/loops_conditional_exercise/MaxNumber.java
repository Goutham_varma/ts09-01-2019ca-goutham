package loops_conditional_exercise;
import java.util.*;
public class MaxNumber {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a number: ");
		int first = userInput.nextInt();
		System.out.println("Enter another number: ");
		int second = userInput.nextInt();
		userInput.close();
		int max = (first>second)?first:second;
		System.out.println("The maximum value is "+max);
	}

}
