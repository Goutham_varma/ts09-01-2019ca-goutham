package loops_conditional_exercise;
import java.util.*;
public class PerfectNumber {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a number: ");
		int number = userInput.nextInt();
		userInput.close();
		int sum = 0;
		for(int i = 1;i<number;i++) {
			if((number%i) ==0) {
				sum = sum + i;
			}
		}
		if(number == sum) {
			System.out.println("Yes, it is a perfect number");
		}
		else {
			System.out.println("No, it is not a perfect number");
		}
	}

}
