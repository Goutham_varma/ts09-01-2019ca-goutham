package loops_conditional_exercise;
import java.util.*;
import java.lang.Math;
public class SwapFirstAndLastDigit {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a number: ");
		long num = userInput.nextLong();
		userInput.close();
		long temp = num/10;
		int count = 1;
		while(temp!=0) {
			count++;
			temp = temp/10;			
		}
		long[] arr = new long[count];
		for(int i = count-1;i>0;i--) {
			arr[i]=num%10;
			if((num/10)!=0) {
				num = num/10;
			}			
		}
		arr[0]=num;
		long swap = arr[0];
		arr[0]=arr[count-1];
		arr[count-1]=swap;
		long x=0;
		for(int i = 0;i<count;i++) {
			x = x + (arr[i] * (long) Math.pow(10,(count-1-i)));
		}
		num = x;
		System.out.println("The number after swapping first and last digit is: "+num);
	}
}
