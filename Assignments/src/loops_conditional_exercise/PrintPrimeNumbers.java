package loops_conditional_exercise;
import java.util.*;
public class PrintPrimeNumbers {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a number up to which the prime numbers are printed: ");
		int num = userInput.nextInt();
		userInput.close();
		int count = 0;
		System.out.println("The primenumbers between 1 and "+num+" are ");
		for(int i = 2;i<=num;i++) {
			for(int j = 1; j<=i;j++) {
				if(i%j == 0) {
					count+=1;
				}
			}
			if(count == 2) {
				System.out.print(i+", ");
			}
			count = 0;
		}
	}
}
