//Prints the alphabets in both small case and capital case
package loops_conditional_exercise;
public class Alphabets {

	public static void main(String[] args) {
		int num = 26;
		int smallValue = 97;
		int capitalValue = 65;
		System.out.println("The alphabets in small case: ");
		while(num>0) {
			System.out.print((char)smallValue);
			smallValue = smallValue+1;
			num = num - 1;
		}
		System.out.println("");
		System.out.println("The alphabets in capital case: ");
		num = 26;
		while(num>0) {
			System.out.print((char)capitalValue);
			capitalValue = capitalValue+1;
			num = num - 1;
		}
	}

}
