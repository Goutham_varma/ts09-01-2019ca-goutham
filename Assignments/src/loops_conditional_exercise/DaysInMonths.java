package loops_conditional_exercise;
import java.util.*;
public class DaysInMonths {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter the name of the month: ");
		String m = userInput.next();
		String month = m.toLowerCase();
		userInput.close();
		switch(month) {
		case "january":
			System.out.println(31+" days in "+month);
			break;
		case "february":
			System.out.println(28+" days in "+month);
			break;
		case "march":
			System.out.println(31+" days in "+month);
			break;
		case "april":
			System.out.println(30+" days in "+month);
			break;
		case "may":
			System.out.println(31+" days in "+month);
			break;
		case "june":
			System.out.println(30+" days in "+month);
			break;
		case "july":
			System.out.println(31+" days in "+month);
			break;
		case "august":
			System.out.println(31+" days in "+month);
			break;
		case "september":
			System.out.println(30+" days in "+month);
			break;
		case "october":
			System.out.println(31+" days in "+month);
			break;
		case "november":
			System.out.println(30+" days in "+month);
			break;
		case "december":
			System.out.println(31+" days in "+month);
			break;
		default:
			System.out.println("Enter a valid month name");
		}
	}

}
