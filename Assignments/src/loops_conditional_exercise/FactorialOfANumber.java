package loops_conditional_exercise;
import java.util.*;
public class FactorialOfANumber {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a number: ");
		int number = userInput.nextInt();
		userInput.close();
		int result=1;
		for(int i = number;i>=1;i--) {
			result = result * i;
		}
		System.out.println("The factorial of "+number+" is "+result);
	}

}
