package loops_conditional_exercise;
import java.util.*;
public class MaxOfThree {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter first number: ");
		int first = userInput.nextInt();
		System.out.println("Enter second number: ");
		int second = userInput.nextInt();
		System.out.println("Enter third number: ");
		int third = userInput.nextInt();
		userInput.close();
		int temp = (first>second)?first:second;
		int max = (third>temp)?third:temp;
		System.out.println("The maximum value is "+max);
	}

}
