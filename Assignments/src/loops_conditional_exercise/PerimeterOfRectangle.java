//program to calculate perimeter of rectangle
package loops_conditional_exercise;
import java.util.Scanner;
public class PerimeterOfRectangle {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter the length of rectangle: ");
		double length = userInput.nextDouble();
		System.out.println("Enter the breadth of rectangle: ");
		double breadth = userInput.nextDouble();
		userInput.close();
		System.out.println("The perimeter of rectangle is "+2*(length+breadth));

	}

}
