//program to convert days to years, months, days
package loops_conditional_exercise;
import java.util.*;
public class DaysConversion {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter the number of days: ");
		int userDays = userInput.nextInt();
		userInput.close();
		int years = userDays/365;
		int months = userDays%365;
		int days = months%30;
		months = months/30;
		System.out.println(years+" Y "+months+" M "+days+" D");
		
	}

}
