package loops_conditional_exercise;
import java.util.*;
public class PrimeFactors {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a number: ");
		long number = userInput.nextLong();
		userInput.close();
		int count = 0;
		System.out.print("Prime Factors of "+number+" are ");
		for(int i=1;i<=number;i++) {
			if(number%i==0) {
				for(int j=1;j<=i;j++) {
					if((i%j) == 0) {
						count+=1;
					}
				}
				if(count==2) {
					System.out.print(i+", ");
				}
			}
			count = 0;
		}

	}

}
