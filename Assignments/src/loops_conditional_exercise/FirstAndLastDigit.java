package loops_conditional_exercise;
import java.util.*;
public class FirstAndLastDigit {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a number: ");
		long num = userInput.nextLong();
		userInput.close();
		long lastDigit = num % 10;
		while((num/10)!=0) {
			num = num / 10;
		}
		long firstDigit = num;
		System.out.println("The last digit of number is "+lastDigit);
		System.out.println("The first digit of number is "+firstDigit);
	}

}
