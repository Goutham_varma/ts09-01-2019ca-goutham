package loops_conditional_exercise;
import java.util.*;
public class FibanocciSeries {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter the number of elements in fibonacci series you want to print ");
		int num = userInput.nextInt();
		userInput.close();
		int temp = 0;
		int sum = 0;
		int var = 1;
		while(num>0) {
			System.out.print(var+", ");
			sum = temp + var;
			temp = var;
			var = sum;
			num = num - 1;
		}
	}

}
