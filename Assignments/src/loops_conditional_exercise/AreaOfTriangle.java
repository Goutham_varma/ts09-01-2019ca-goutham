//program to find the area of triangle given base and height
package loops_conditional_exercise;
import java.util.*;
public class AreaOfTriangle {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter the base of the triangle: ");
		double base = userInput.nextDouble();
		System.out.println("Enter the height of the triangle: ");
		double height = userInput.nextDouble();
		userInput.close();
		double area = (1.0/2.0)*base*height;
		System.out.println("Area of the triangle is "+area);
	}

}
