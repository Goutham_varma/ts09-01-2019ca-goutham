package loops_conditional_exercise;
import java.util.*;
public class SquareRoot {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a number: ");
		double num = userInput.nextDouble();
		userInput.close();
		double squareRoot = num/2;
		double temp;
		do {
			temp = squareRoot;
			squareRoot = (temp + (num/temp))/2;
		}
		while((temp-squareRoot)!=0);
		System.out.println("The square root of "+num+" is "+squareRoot);
	}

}
