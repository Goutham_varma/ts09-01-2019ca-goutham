//prints out all the ascii values
package loops_conditional_exercise;

public class AsciiValues {

	public static void main(String[] args) {
		for(int i=0;i<=255;i++) {
			System.out.println("The ASCII value of "+(char)i+" is "+i);
		}

	}

}
