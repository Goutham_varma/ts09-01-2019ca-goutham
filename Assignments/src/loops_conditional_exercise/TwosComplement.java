package loops_conditional_exercise;
import java.util.*;
public class TwosComplement {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a binary number: ");
		String binaryNumber = userInput.nextLine();
		userInput.close();
		int index = 0;
		StringBuffer output = new StringBuffer();
		for(int i = 0;i<binaryNumber.length();i++) {
			if (binaryNumber.charAt(i)=='1') {
				output.append('0');
			}
			else if(binaryNumber.charAt(i)=='0'){
				output.append('1');
			}
		}
		StringBuffer twos_output = new StringBuffer();
		int temp = output.length()-1;
		while(temp>=0) {
			if(output.charAt(temp)!='0') {
				twos_output.append('0');
			}
			else {
				twos_output.append('1');
				index = temp;
				break;
			}
			temp = temp - 1;
		}
		for(int i = index-1;i>=0;i--) {
			twos_output.append(output.charAt(i));
		}
		System.out.print("The two's complement of the given binary number is ");
		for(int i = twos_output.length()-1;i>=0;i--) {
			System.out.print(twos_output.charAt(i));
		}
	}

}
