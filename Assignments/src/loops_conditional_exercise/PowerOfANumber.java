package loops_conditional_exercise;
import java.util.*;
public class PowerOfANumber {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a number: ");
		int firstNum = userInput.nextInt();
		System.out.println("Enter another number: ");
		int secondNum = userInput.nextInt();
		userInput.close();
		int result = 1;
		for(int i = 0;i<secondNum;i++) {
			result = result * firstNum;
		}
		System.out.println("The "+firstNum+" raised to the power "+secondNum+" is "+result);
	}

}