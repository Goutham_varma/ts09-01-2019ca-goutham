package loops_conditional_exercise;
import java.util.*;
public class Marks {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		double[] arr = new double[5];
		System.out.println("Enter the marks in the subjects: ");
		for(int i=0;i<5;i++) {
			arr[i]= userInput.nextDouble();
		}
		userInput.close();
		double sum = 0;
		for(int i=0;i<5;i++) {
			sum = sum+arr[i];
		}
		System.out.println("The total marks are "+sum);
		double average = sum/5;
		System.out.println("The average mark of five subjects is "+average);
		double percentage = (sum/500)*100;
		System.out.println("The percentage attained is "+percentage+" %");
	}

}
