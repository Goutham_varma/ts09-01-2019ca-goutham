//Program to convert temperature from celsius to fahrenheit
package loops_conditional_exercise;
import java.util.*;
public class CelsiusToFarhenheit {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter the temperature in celsius: ");
		double celsius = userInput.nextDouble();
		userInput.close();
		double fahrenheit = ((9*celsius)/5)+32;
		System.out.println("The temperature in farhenheit is "+fahrenheit);
	}
}
