//program to convert temperature from fahrenheit to celsius
package loops_conditional_exercise;
import java.util.*;
public class FarhenheitToCelsius {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter the temperature in Farhenheit: ");
		double fahrenheit = userInput.nextDouble();
		userInput.close();
		double celsius = ((fahrenheit-32)/9)*5;
		System.out.println("The temperature in farhenheit is "+celsius);
	}
}
