//Takes an input number between 0 and 6 and prints the day of the week
package loops_conditional_exercise;
import java.util.*;
public class DayOfWeek {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a number: ");
		int x = userInput.nextInt();
		userInput.close();
		switch(x) {
		case 1:
			System.out.println("Monday");
			break;
		case 2:
			System.out.println("Tuesday");
			break;
		case 3:
			System.out.println("Wednesday");
			break;
		case 4:
			System.out.println("Thursday");
			break;
		case 5:
			System.out.println("Friday");
			break;
		case 6:
			System.out.println("Saturday");
			break;
		case 0:
			System.out.println("Sunday");
			break;
		default:
			System.out.println("Enter a number from 0 to 6");
		}
	}

}
