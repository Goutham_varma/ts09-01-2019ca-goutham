//Calculator with addition, subtraction, multiplication and divison
package loops_conditional_exercise;
import java.util.*;
public class Calculator {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a number: ");
		double first = userInput.nextDouble();
		System.out.println("Enter another number: ");
		double second = userInput.nextDouble();
		System.out.println("Choose one of these numbers (add:0,sub:1,mul:2,div:3): ");
		int arithmetic = userInput.nextInt();
		userInput.close();
		switch(arithmetic) {
		case 0:
			System.out.println("The result after adding the numbers is "+(first+second));
			break;
		case 1:
			System.out.println("The result after subtracting the numbers is "+(first-second));
			break;
		case 2:
			System.out.println("The result after multiplying the numbers is "+(first*second));
			break;
		case 3:
			System.out.println("The result after dividing the numbers is "+(first/second));
			break;
		default:
			System.out.println("Enter valid code (add:0,sub:1,mul:2,div:3)");
		}
	}

}
