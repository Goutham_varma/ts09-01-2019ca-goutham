package loops_conditional_exercise;

import java.util.*;

public class ProductOfDigits {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a number: ");
		long number = userInput.nextLong();
		userInput.close();
		long product = 1;
		while(number!=0) {
			product = product * (number%10);
			number = number/10;
		}
		System.out.println("The sum of the digits in the number is "+product);

	}

}
