package loops_conditional_exercise;
import java.util.*;
public class VowelCheck {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter an alphabet: ");
		String a = userInput.next();
		String alphabet = a.toLowerCase();
		userInput.close();
		switch(alphabet.charAt(0)) {
		case 'a':
			System.out.println("Vowel");
			break;
		case 'e':
			System.out.println("Vowel");
			break;
		case 'i':
			System.out.println("Vowel");
			break;
		case 'o':
			System.out.println("Vowel");
			break;
		case 'u':
			System.out.println("Vowel");
			break;
		default:
			System.out.println("Consonant");	
		}
	}

}
