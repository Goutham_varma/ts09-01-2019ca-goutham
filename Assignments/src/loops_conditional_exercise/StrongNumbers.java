package loops_conditional_exercise;
import java.util.*;
public class StrongNumbers {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a number up to which you want to print strong numbers: ");
		int num = userInput.nextInt();
		userInput.close();
		int sum;
		int fact;
		int digit;
		int temp;
		System.out.println("The strong numbers between 1 and "+num+" are ");
		for(int i = 1;i<=num;i++) {
			sum = 0;
			temp = i;
			while(temp>0) {
				digit = temp%10;
				fact = 1;
				for(int j=digit;j>=1;j--) {
					fact = fact * j;
				}
				sum = sum + fact;
				temp = temp/10;
			}
			if(i == sum) {
				System.out.print(i+", ");
			}
		}
	}

}
