package loops_conditional_exercise;
import java.util.*;
public class StrongNumberCheck {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a number: ");
		int number = userInput.nextInt();
		userInput.close();
		int n = number;
		int digit;
		int fact;
		int sum=0;
		while(n>0) {
			digit = n%10;
			fact = 1;
			for(int i = digit;i>=1;i--) {
				fact = fact * i;
			}
			sum = sum + fact;
			n = n/10;
		}
		if(number == sum) {
			System.out.println("Yes, it is a strong number");
		}
		else {
			System.out.println("No, it is not a strong number");
		}
	}

}
