package loops_conditional_exercise;
import java.util.*;
public class NaturalNumbers {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter the number of natural numbers you want to print: ");
		int num = userInput.nextInt();
		userInput.close();
		int i = 1;
		while(num>0) {
			System.out.println(i);
			i = i+1;
			num = num-1;
		}
	}
}
