package loops_conditional_exercise;
import java.util.*;
public class FrequencyOfEachDigit {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a number: ");
		String number = userInput.next();
		userInput.close();
		StringBuffer noRepeat = new StringBuffer();
		noRepeat.append(number.charAt(0));
		int temp;
		int count;
		for(int i = 1;i<number.length();i++) {
			temp=0;
			for(int j=0;j<noRepeat.length();j++) {
				if(noRepeat.charAt(j)==number.charAt(i)) {
					temp+=1;
				}
			}
			if(temp==0) {
				noRepeat.append(number.charAt(i));
			}
		}
		System.out.println(noRepeat);
		for(int i = 0;i<noRepeat.length();i++) {
			count = 0;
			for(int j=0;j<number.length();j++) {
				if(noRepeat.charAt(i)==number.charAt(j)) {
					count +=1;
				}
			}
			System.out.println("The number "+noRepeat.charAt(i)+" is repeated "+count+" number of times");
		}
	}
}
