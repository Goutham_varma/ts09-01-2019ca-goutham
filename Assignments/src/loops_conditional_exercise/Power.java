package loops_conditional_exercise;
import java.lang.Math;
import java.util.*;
public class Power {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a number: ");
		double firstNum = userInput.nextDouble();
		System.out.println("Enter another number: ");
		double secondNum = userInput.nextDouble();
		userInput.close();
		double result = Math.pow(firstNum, secondNum);
		System.out.println("The "+firstNum+" raised to the power "+secondNum+" is "+result);
	}

}
