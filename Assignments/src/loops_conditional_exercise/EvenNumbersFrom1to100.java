package loops_conditional_exercise;

public class EvenNumbersFrom1to100 {

	public static void main(String[] args) {
		int num = 1;
		System.out.println("The even numbers from 1 to 100 are: ");
		while(num<101) {
			if(num%2 == 0) {
				System.out.println(num);
			}
			num = num+1;
		}

	}

}
