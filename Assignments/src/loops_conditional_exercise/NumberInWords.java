package loops_conditional_exercise;
import java.util.*;
public class NumberInWords {
	String[] ones_place = {
			"zero","one","two","three","four","five","six","seven","eight","nine","ten","eleven",
			"twelve","thirteen","fourteen","fifteen","sixteen","seventeen","eighteen",
			"nineteen"
	};
	String[] tens_place = {
			"","","twenty","thirty","forty","fifty","sixty","seventy","eighty","ninety"
	};
	String LessThanTwenty(int number) {
		return (ones_place[number]);
	}
	String LessThanHundred(int number) {
		if(number<20) {
			return LessThanTwenty(number);	
		}
		else {
			return (tens_place[number/10]+" "+ones_place[number%10]);
		}
	}
	String LessThanThousand(int number) {
		return (ones_place[number/100]+" hundred and "+LessThanHundred(number%100));
	}
	String LessThanLakh(int number) {
		return (LessThanHundred(number/1000)+" thousand "+LessThanThousand(number%1000));
	}
	String LessThanCrore(int number) {
		return (LessThanHundred(number/100000)+" Lakh "+LessThanLakh(number%100000));
	}
	String MoreThanCrore(int number) {
		return (LessThanHundred(number/10000000)+" crore "+LessThanCrore(number%10000000));
	}
	public static void main(String[] args) {
		System.out.println("Enter a number: ");
		Scanner userInput = new Scanner(System.in);
		try {
			int number = userInput.nextInt();
			userInput.close();	
			NumberInWords word = new NumberInWords();
			if(number<20) {
				String result = word.LessThanTwenty(number);
				System.out.println(result);
			}
			else if((number>=20) && (number<100)) {
				String result = word.LessThanHundred(number);
				System.out.println(result);
			}	
			else if((number>=100) && (number<1000)) {
				String result = word.LessThanThousand(number);
				System.out.println(result);
			}
			else if((number>=1000)&&(number<100000)) {
				String result = word.LessThanLakh(number);
				System.out.println(result);
			}
			else if((number>=100000)&&(number<10000000)) {
				String result = word.LessThanCrore(number);
				System.out.println(result);
			}
			else {
				String result = word.MoreThanCrore(number);
				System.out.println(result);
			}
		}
		catch(InputMismatchException ime) {
			System.out.println("Enter a smaller number than this");
		}
		}
}
