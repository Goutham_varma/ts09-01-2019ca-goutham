package loops_conditional_exercise;
import java.util.*;
public class LCMOfNumbers {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a number: ");
		int firstNum = userInput.nextInt();
		int first = firstNum;
		System.out.println("Enter another number: ");
		int secondNum = userInput.nextInt();
		int second = secondNum;
		userInput.close();
		int temp;
		int hcf;
		int lcm;
		if(firstNum>=secondNum) {
			while((firstNum%secondNum)!=0) {
				temp = secondNum;
				secondNum = firstNum % secondNum;
				firstNum = temp;
			}
			hcf = secondNum;
		}
		else {
			while((secondNum%firstNum)!=0) {
				temp = firstNum;
				firstNum = secondNum % firstNum;
				secondNum = temp;
			}
			hcf = firstNum;
		}
		int var = first/hcf;
		lcm = var * second;
		System.out.println("The LCM of two numbers is "+lcm);
	}

}
