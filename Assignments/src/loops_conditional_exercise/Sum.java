//program to calculate sum of two numbers
package loops_conditional_exercise;
import java.util.*;

public class Sum {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a number: ");
		double firstNum = userInput.nextDouble();
		System.out.println("Enter another number: ");
		double secondNum = userInput.nextDouble();
		userInput.close();
		System.out.println("The sum of the numbers is "+(firstNum + secondNum));

	}

}
