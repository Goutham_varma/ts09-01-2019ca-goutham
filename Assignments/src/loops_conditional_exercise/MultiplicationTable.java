package loops_conditional_exercise;
import java.util.*;
public class MultiplicationTable {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter the number for which you want the table: ");
		int num = userInput.nextInt();
		userInput.close();
		System.out.println("The multiplication table of "+num+" is ");
		for(int i =1;i<=10;i++) {
			System.out.println(num+" x "+i+" = "+(num*i));
		}
	}

}
