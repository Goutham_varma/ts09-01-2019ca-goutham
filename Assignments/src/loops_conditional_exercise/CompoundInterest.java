//program to calculate the compound interest
package loops_conditional_exercise;
import java.util.*;
import java.lang.Math;
public class CompoundInterest {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter the principle amount: ");
		double P = userInput.nextDouble();
		System.out.println("Enter the time in months: ");
		int T = userInput.nextInt() / 12;
		System.out.println("Enter the annual rate of interest in percentage: ");
		double R = userInput.nextDouble()/100;
		System.out.println("Enter the number of time per year interest is compounded: ");
		int n = userInput.nextInt();
		userInput.close();
		double temp = Math.pow((1+(R/n)), (n*T));
		double tCi = P * (temp - 1);
		System.out.println("The compound interest is "+tCi);
	}

}
