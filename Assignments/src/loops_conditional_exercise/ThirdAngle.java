package loops_conditional_exercise;
import java.util.*;
public class ThirdAngle {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter first angle: ");
		double firstAngle = userInput.nextDouble();
		System.out.println("Enter second angle: ");
		double secondAngle = userInput.nextDouble();
		userInput.close();
		System.out.println("The third of the triangle is "+(180-(firstAngle+secondAngle)));

	}

}
