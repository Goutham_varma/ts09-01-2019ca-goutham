//Prints out the armstrong numbers between a range
package loops_conditional_exercise;
import java.util.*;
import java.lang.Math;
public class ArmstrongNumbers {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a number upto which you want all armstrong numbers: ");
		int number = userInput.nextInt();
		userInput.close();
		int sum = 0;
		int digit;
		int n;
		System.out.println("The armstrong numbers between 1 and "+number+" are ");
		for(int num =0;num<=number;num++){
			n = num;
			while(n>0) {
				digit = n%10;
				sum = sum + (int)Math.pow(digit, 3);
				n = n/10;
			}
			if(num == sum) {
				System.out.print(num+", ");
			}
			sum = 0;
		}
	}
}
