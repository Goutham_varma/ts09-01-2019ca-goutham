package loops_conditional_exercise;
import java.util.*;
public class PalindromeCheck {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a number: ");
		String num = userInput.nextLine();
		userInput.close();
		StringBuffer reverseNum = new StringBuffer();
		for(int i=num.length()-1;i>=0;i--) {
			reverseNum.append(num.charAt(i));
		}
		String reverse = reverseNum.toString();
		if(reverse.equals(num)) {
			System.out.println("Yes, the number is a palindrome");
		}
		else {
			System.out.println("No, the number is not a palindrome");
		}
		
	}

}
