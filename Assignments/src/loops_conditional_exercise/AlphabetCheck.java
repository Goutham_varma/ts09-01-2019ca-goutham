//Checks if the user input is an alphabet or not
package loops_conditional_exercise;
import java.util.*;
public class AlphabetCheck {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a character: ");
		String input = userInput.next(); //user input is scanned
		char c = input.charAt(0); 
		int value = (int) c;
		userInput.close(); //scanner is closed
		System.out.println((((64<value)&&(value<91)) || ((96<value)&&(value<123)))?"Alphabet":"Not an alphabet");
	}

}
