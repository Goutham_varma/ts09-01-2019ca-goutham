package loops_conditional_exercise;
import java.util.*;
import java.lang.Math;
public class RootsOfQuadraticEquation {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter the coefficients of the equation: ");
		System.out.println("Enter a: ");
		double a = userInput.nextDouble();
		System.out.println("Enter b: ");
		double b = userInput.nextDouble();
		System.out.println("Enter c: ");
		double c = userInput.nextDouble();
		userInput.close();
		double det = b*b - (4*a*c);
		int flag = 0;
		if(det>0) {
			flag = 1;
		}
		else if(det<0) {
			flag = -1;
		}
		else {
			flag = 0;
		}
		switch(flag) {
		case 1:
			System.out.println("The roots are "+(Math.sqrt(det)-b)/(2*a)+", "+(-Math.sqrt(det)-b)/(2*a));
			break;
		case -1:
			System.out.println("The roots are "+(-b)/(2*a)+"+i"+(Math.sqrt(-det))/(2*a)+", "+(-b)/(2*a)+"-i"+(Math.sqrt(-det))/(2*a));
			break;
		default:
			System.out.println("The roots are "+(-b)/(2*a)+", "+(-b)/(2*a));
		}
	}

}
