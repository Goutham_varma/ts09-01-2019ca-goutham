package loops_conditional_exercise;
import java.util.*;
public class ReversePrintNum {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a number: ");
		String num = userInput.next();
		userInput.close();
		System.out.println("The number in the reverse is: ");
		for(int i=num.length()-1;i>=0;i--) {
			System.out.print(num.charAt(i));
		}
	}

}
