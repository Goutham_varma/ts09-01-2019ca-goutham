package loops_conditional_exercise;
import java.util.*;
public class SimpleInterest {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter the principle amount: ");
		double P = userInput.nextDouble();
		System.out.println("Enter the time in months: ");
		int T = userInput.nextInt() / 12;
		System.out.println("Enter the rate of interest in percentage: ");
		double R = userInput.nextDouble();
		userInput.close();
		double sI = (P * T * R)/100;
		System.out.println("The simple interest is "+sI);
	}

}
