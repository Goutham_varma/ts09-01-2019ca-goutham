package loops_conditional_exercise;
import java.util.*;
public class PerfectNumbers {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a number up to which you want to print perfect numbers: ");
		int num = userInput.nextInt();
		userInput.close();
		int sum;
		System.out.println("The perfect numbers between 1 and "+num+" are ");
		for(int i = 1; i <=num;i++) {
			sum = 0;
			for(int j = 1;j<i;j++) {
				if(i%j == 0) {
					sum = sum+j;
				}
			}
			if(sum == i) {
				System.out.print(i+", ");
			}
		}
	}

}
