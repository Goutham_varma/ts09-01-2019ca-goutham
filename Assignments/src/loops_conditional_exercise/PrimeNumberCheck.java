package loops_conditional_exercise;
import java.util.*;
public class PrimeNumberCheck {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a number: ");
		int number = userInput.nextInt();
		userInput.close();
		int count=0;
		for(int i=1;i<=number;i++) {
			if((number%i) == 0) {
				count+=1;
			}
		}
		if(count==2) {
			System.out.println("Yes, it is a prime number");
		}
		else {
			System.out.println("No, it is not a prime number");
		}
	}

}
