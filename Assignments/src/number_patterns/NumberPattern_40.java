package number_patterns;

public class NumberPattern_40 {

	public static void main(String[] args) {
		for(int i =0;i<5;i++) {
			for(int j =1;j<=2*i+1;j=j+2) {
				System.out.print(j);
			}
			for(int k = 2*i-1;k>0;k=k-2) {
				System.out.print(k);
			}
			System.out.println("");
		}

	}

}
