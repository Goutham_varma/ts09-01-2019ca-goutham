package number_patterns;

public class NumberPattern_41 {

	public static void main(String[] args) {
		for(int i =0;i<5;i++) {
			for(int j =2;j<=2*i+2;j=j+2) {
				System.out.print(j);
			}
			for(int k = 2*i;k>0;k=k-2) {
				System.out.print(k);
			}
			System.out.println("");
		}

	}

}
