package number_patterns;

public class NumberPattern_17 {

	public static void main(String[] args) {
		for(int i=5;i>=1;i--) {
			for(int j=5;j>i;j--) {
				System.out.print(j);
			}
			for(int k=1;k<=(2*i-1);k++) {
				System.out.print(i);
			}
			for(int l=i+1;l<6;l++) {
				System.out.print(l);
			}
			System.out.println("");
		}
		for(int i = 2;i<=5;i++) {
			for(int k=5;k>=i;k--) {
				System.out.print(k);
			}
			for(int j = 1;j<=2*i-3;j++) {
				System.out.print(i);
			}
			for(int l = i;l<=5;l++ ) {
				System.out.print(l);
			}
			System.out.println("");
		}

	}

}
