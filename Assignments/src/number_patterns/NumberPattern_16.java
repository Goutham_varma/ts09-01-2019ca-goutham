package number_patterns;

public class NumberPattern_16 {

	public static void main(String[] args) {
		for(int i=0;i<5;i++) {
			for(int k =i+1;k>1;k--) {
				System.out.print(k);
			}
			for(int j=1;j<=5-i;j++) {
				System.out.print(j);
			}
			System.out.println();
		}
	}

}
