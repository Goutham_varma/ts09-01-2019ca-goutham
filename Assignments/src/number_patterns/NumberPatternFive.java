package number_patterns;

public class NumberPatternFive {

	public static void main(String[] args) {
		for(int i =0;i<5;i++) {
			if(i==0||i==2||i==4) {
				System.out.print("10101");
			}
			else {
				System.out.print("01010");
			}
			System.out.println("");
		}

	}

}
