package number_patterns;

public class NumberPattern_58 {

	public static void main(String[] args) {
		for(int i = 0;i<9;i++) {
			if(i==0 || i==8) {
				System.out.print("1       1");
			}
			else if(i==1 || i==7) {
				System.out.print(" 2     2 ");
			}
			else if(i==2 || i==6) {
				System.out.print("  3   3  ");
			}
			else if(i==3 || i==5) {
				System.out.print("   4 4   ");
			}
			else {
				System.out.print("    5    ");
			}
			System.out.println("");
		}

	}

}
