package number_patterns;

public class NumberPattern_51 {

	public static void main(String[] args) {
		int val = 1;
		for(int i = 1;i<=5;i++) {
			for(int j = 1;j<=i;j++) {
				System.out.print(val);
			}
			if(i>=3) {
				val = val -1;
			}
			else {
				val=val+1;
			}
			
			System.out.println("");
		}

	}

}
