package number_patterns;

public class NumberPatternEight {

	public static void main(String[] args) {
		for(int i=0;i<5;i++) {
			if(i==0||i==4) {
				System.out.print("01110");
			}
			else {
				System.out.print("10001");
			}
			System.out.println("");
		}
	}

}
