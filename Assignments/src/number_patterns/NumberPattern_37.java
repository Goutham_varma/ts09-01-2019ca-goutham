package number_patterns;

public class NumberPattern_37 {

	public static void main(String[] args) {
		for(int i=0;i<5;i++) {
			if(i==2) {
				System.out.print("101");
			}
			else if(i==3) {
				System.out.print("1001");
			}
			else {
				for(int j=0;j<=i;j++) {
					System.out.print(1);
				}
			}
			System.out.println("");
		}

	}

}
