package number_patterns;

public class NumberPattern_15 {

	public static void main(String[] args) {
		for(int i=0;i<5;i++) {
			for(int j=i+1;j<=5;j++) {
				System.out.print(j);
			}
			for(int k=i;k>0;k--) {
				System.out.print(k);
			}
			System.out.println();
		}

	}

}
