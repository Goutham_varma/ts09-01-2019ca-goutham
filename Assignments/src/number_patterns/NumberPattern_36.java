package number_patterns;

public class NumberPattern_36 {

	public static void main(String[] args) {
		int temp = 1;
		for(int i = 1;i<=5;i++) {
			for(int j = 1;j<=i;j++) {
				System.out.print(temp);
				if(temp == 1) {
					temp = 0;
				}
				else {
					temp = 1;
				}
			}
			System.out.println("");
		}


	}

}
