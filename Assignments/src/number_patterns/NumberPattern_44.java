package number_patterns;

public class NumberPattern_44 {

	public static void main(String[] args) {
		int n = 1;
		for(int i = 1;i<=5;i++) {
			for(int j = i;j<=2*i-1;j++) {
				System.out.print(n+" ");
				n = n+1;
			}
			System.out.println("");
		}

	}

}
