package number_patterns;

public class NumberPattern_49 {

	public static void main(String[] args) {
		int var = 1;
		int temp = 1;
		for(int i = 1;i<=5;i++) {
			for(int j = i;j>=1;j--) {
				System.out.print(var+" ");
				var = var + temp;
				temp = temp+1;
			}
			System.out.println("");
		}

	}

}
