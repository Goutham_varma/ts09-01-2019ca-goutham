package number_patterns;

public class NumberPatternThree {

	public static void main(String[] args) {
		for(int i = 0;i<5;i++) {
			if(i==0 || i==4) {
				for(int j = 0;j<5;j++) {
					System.out.print(1);
				}
			}
			else {
				System.out.print("10001");
			}
			System.out.println("");
		}

	}

}
