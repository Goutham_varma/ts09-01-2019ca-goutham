package number_patterns;

public class NumberPattern_18 {

	public static void main(String[] args) {
		int[][] arr = new int[5][5];
		int left = 0;
		int top = 4;
		int value = 1;
		for(int i = 1;i<=3;i++) {
			for(int j =left;j<=top;j++ ) {
				arr[left][j] = value;
				value = value+1;
			}
			for(int j = left+1;j<=top;j++) {
				arr[j][top] = value;
				value = value+1;
			}
			for(int j = top-1;j>=left;j--) {
				arr[top][j] = value;
				value = value+1;
			}
			for(int j = top-1;j>=left+1;j--) {
				arr[j][left] = value;
				value = value+1;
			}
			left = left+1;
			top = top-1;
		}
		for(int i =0;i<5;i++) {
			for(int j = 0;j<5;j++) {
				System.out.print(arr[i][j]+" ");
			}
			System.out.println("");
		}
	}

}
