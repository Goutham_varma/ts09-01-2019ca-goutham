package number_patterns;

public class NumberPatternSeven {

	public static void main(String[] args) {
		for(int i=0;i<5;i++) {
			if(i==0||i==4) {
				System.out.print("10001");
			}
			else if(i==1||i==3) {
				System.out.print("01010");
			}
			else {
				System.out.print("00100");
			}
			System.out.println("");
		}

	}

}
