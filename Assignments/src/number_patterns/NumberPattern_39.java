package number_patterns;

public class NumberPattern_39 {

	public static void main(String[] args) {
		for(int i = 1;i<=5;i++) {
			if(i%2 == 0) {
				for(int j = 2;j<=2*i;j=j+2) {
					System.out.print(j);
				}
			}
			else {
				for(int j = 1;j<=2*i-1;j=j+2) {
					System.out.print(j);
				}
			}
			System.out.println("");
			
		}

	}

}
