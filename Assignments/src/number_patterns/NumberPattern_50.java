package number_patterns;

public class NumberPattern_50 {

	public static void main(String[] args) {
		int temp = 0;
		int val;
		for(int i = 1;i<=5;i++) {
			if(i%2!=0) {
				val = temp+1;
			}
			else {
				val = temp+i;
			}
			for(int j = 1;j<=i;j++) {
				System.out.print(val+" ");
				if(i%2==0) {
					val = val-1;
				}
				else {
					val = val+1;
				}
				temp=temp+1;
			}
			System.out.println("");
		}

	}

}
