package number_patterns;

public class NumberPattern_48 {

	public static void main(String[] args) {
		int var = 4;
		int k;
		System.out.println(1);
		for(int i = 2;i<=5;i++) {
			k = i;
			for(int j = i;j>=1;j--) {
				System.out.print(k+" ");
				k = k+var;
				var = var-1;
			}
			var = 4;
			System.out.println("");
		}
	}

}
