package classpractice;

import java.util.*;

public class ReverseSorting {
	static public int[] arr;
	public void rSort() {
		for(int n = 0;n<(arr.length-1);n++){
			int index = n;
			int max = arr[n];
			for(int i = (arr.length-1);i>=n;i--) {
				if(arr[i]>max) {
					max = arr[i];
					index = i;
				}
			}
			int temp = arr[n];
			arr[index] = temp;
			arr[n] = max;
		}
	}
	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter the number of elements to sort: ");
		int num = userInput.nextInt();
		arr = new int[num];
		System.out.println("Enter "+num+" numbers: ");
		for(int i=0;i<num;i++) {
			arr[i] = userInput.nextInt();
 		}
		userInput.close();
		ReverseSorting object_one = new ReverseSorting();
		object_one.rSort();
		System.out.println("Sorted numbers are: ");
		for(int i = 0;i<num;i++) {
			System.out.print(arr[i]+", ");
		}

	}

}
