package classpractice;
import java.util.Arrays;
import java.util.Scanner;
public class ArraySorting {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter the number of elements to sort: ");
		int num = userInput.nextInt();
		int[] arr = new int[num];
		System.out.println("Enter "+num+" numbers: ");
		for(int i=0;i<num;i++) {
			arr[i] = userInput.nextInt();
 		}
		userInput.close();
		Arrays.sort(arr);
		System.out.println("Sorted numbers are: ");
		for(int i = 0;i<num;i++) {
			System.out.print(arr[i]+", ");
		}
	}

}
