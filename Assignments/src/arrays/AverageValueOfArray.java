package arrays;
import java.util.*;
public class AverageValueOfArray {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter the number of values you want to store: ");
		int size = userInput.nextInt();
		int[] arr = new int[size];
		System.out.println("Enter "+size+" Values: ");
		float sum = 0;
		for(int i = 0;i<size;i++) {
			arr[i] = userInput.nextInt();
			sum = sum+arr[i];
		}
		userInput.close();
		System.out.println(sum);
		System.out.println("The average of the values in the array is "+(sum/size));

	}

}
