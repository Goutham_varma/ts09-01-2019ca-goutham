package classpractice;

import java.util.*;

public class BubbleSort {
	static public int[] arr;
	public void bSort() {
		for(int n = (arr.length-1);n>0;n--){
			int index = 0;
			int max = arr[0];
			for(int i = 0;i<=n;i++) {
				if(arr[i]>max) {
					max = arr[i];
					index = i;
				}
			}
			int temp = arr[n];
			arr[index] = temp;
			arr[n] = max;
		}
	}
	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter the number of elements to sort: ");
		int num = userInput.nextInt();
		arr = new int[num];
		System.out.println("Enter "+num+" numbers: ");
		for(int i=0;i<num;i++) {
			arr[i] = userInput.nextInt();
 		}
		userInput.close();
		BubbleSort object_one = new BubbleSort();
		object_one.bSort();
		System.out.println("Sorted numbers are: ");
		for(int i = 0;i<num;i++) {
			System.out.print(arr[i]+", ");
		}

	}

}
