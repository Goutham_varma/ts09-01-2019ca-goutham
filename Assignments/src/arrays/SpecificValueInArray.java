package arrays;
import java.util.*;
public class SpecificValueInArray {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a number between 0 and 9 ");
		int input = userInput.nextInt();
		userInput.close();
		int[] arr = {0,3,4,6,9};
		int check = 0;
		for(int i = 0;i<arr.length;i++) {
			if(arr[i]==input) {
				check=check+1;
			}
		}
		if(check == 0) {
			System.out.println("No, the number is not in the array");
		}
		else {
			System.out.println("Yes, the number is in the array");
		}

	}

}
