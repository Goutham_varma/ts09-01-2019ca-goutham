//Problem number 6

/*
 * A Java program to print 'Hello' on screen and then print your name on a separate line
 */

package assignment_one;

public class Hello {
	public static void main(String[] args) {
		System.out.println("Hello");
		System.out.println("Goutham Varma");
	}
}
