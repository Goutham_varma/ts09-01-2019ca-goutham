//Problem number 10


//a Java program to convert seconds to hour, minute and seconds


package assignment_one;
import java.util.Scanner;
public class TimeConversions {
	int hour;
	int minute;
	int second;
	public void time(int seconds) {
		second = seconds % 60; 
		minute = seconds / 60;
		hour = minute / 60;
		minute = minute % 60;
		System.out.println("The time in hours, minutes and seconds is "+hour+" : "+minute+" : "+second);
	}
	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter the number of seconds: ");
		int seconds = userInput.nextInt();
		userInput.close();
		TimeConversions object_one = new TimeConversions();
		object_one.time(seconds);
	}
}