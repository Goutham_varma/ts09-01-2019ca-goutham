//Problem number 2

//A Java program that takes two numbers as input and display the product of two numbers

package assignment_one; 
import java.util.Scanner;
public class Product {
	
	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter two integers: ");
		int first_number = userInput.nextInt();
		int second_number = userInput.nextInt();
		userInput.close();
		System.out.println("The product of the two numbers is "+first_number*second_number);
	}
}
