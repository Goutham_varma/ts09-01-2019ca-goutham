//Problem number 9

//A Java program to print the area and perimeter of a rectangle


package assignment_one;
import java.util.Scanner;
public class AreaPerimeterRect {
	int area(int length, int breadth){
		return length * breadth;
	}
	int perimeter(int length, int breadth) {
		return 2 * (length + breadth);
	}
	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter the length of rectangle: ");
		int length = userInput.nextInt();
		System.out.println("Enter the breadth of rectangle: ");
		int breadth = userInput.nextInt();
		userInput.close();
		AreaPerimeterRect object_one = new AreaPerimeterRect();
		System.out.println("The area of the rectangle is " + object_one.area(length, breadth));
		System.out.println("The perimeter of the rectangle is " + object_one.perimeter(length, breadth));
		}
}
