//Problem number 3

/*
 * A Java program to print the sum (addition), multiply, subtract, divide 
	and remainder of two numbers
 */


package assignment_one;
import java.util.Scanner;


public class Arithmetic {
	int add(int first_num, int second_num) {
		return first_num + second_num;
	}
	int product(int first_num, int second_num) {
		return first_num * second_num;
	}
	int subtract(int first_num, int second_num) {
		return first_num - second_num;
	}
	float divide(float first_num, float second_num) {
		return first_num/second_num;
	}
	int returnRemainder(int first_num, int second_num) {
		return first_num % second_num;
	}
	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter two integers: ");
		int first_num = userInput.nextInt();
		int second_num = userInput.nextInt();
		userInput.close();
		Arithmetic object_one = new Arithmetic();
		int add_result = object_one.add(first_num, second_num);
		int sub_result = object_one.subtract(first_num, second_num);
		int mul_result = object_one.product(first_num, second_num);
		float div_result = object_one.divide((float) first_num, (float) second_num);
		int mod_result = object_one.returnRemainder(first_num, second_num);
		System.out.println("Result after adding the two numbers is: "+add_result);
		System.out.println("Result after subtracting the two numbers is: "+sub_result);
		System.out.println("Result after multiplying the two numbers is: "+mul_result);
		System.out.println("Result after dividing the two numbers is: "+div_result);
		System.out.println("Remainder after dividing the two numbers is: "+mod_result);
		
	}
}
