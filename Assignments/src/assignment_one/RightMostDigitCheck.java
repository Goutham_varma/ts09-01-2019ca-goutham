// Problem number 4

/*
 * a Java program that accepts three integers from the user and return true 
 * if two or more of them (integers ) have the same rightmost digit. 
 * The integers are non-negative
 */

package assignment_one;
import java.util.Scanner;
public class RightMostDigitCheck {
	/**
	 * @param first_num
	 * @param second_num
	 * @param third_num
	 */
	/**
	 * @param first_num
	 * @param second_num
	 * @param third_num
	 */
	void check(int first_num, int second_num, int third_num) {
		int rd_one = first_num % 10;
		int rd_two = second_num % 10;
		int rd_three = third_num % 10;
		if ((rd_one == rd_two) && (rd_two == rd_three)) {
			System.out.println("True");
		}
		else if((rd_one == rd_two) || (rd_two == rd_three) || (rd_one == rd_two)) {
			System.out.println("True");
		}
		else {
			System.out.println("False");
		}
	}
	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter three non-negative integers: ");
		int first_num = userInput.nextInt();
		int second_num = userInput.nextInt();
		int third_num = userInput.nextInt();
		userInput.close();
		RightMostDigitCheck object_one = new RightMostDigitCheck();
		object_one.check(first_num, second_num, third_num);
}
}