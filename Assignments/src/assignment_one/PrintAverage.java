//Problem number 8 

/*
 * a Java program that takes five numbers as input to 
 * calculate and print the average of the numbers
 */


package assignment_one;
import java.util.Scanner;

public class PrintAverage {
	float average(int num_one, int num_two, int num_three, int num_four, int num_five) {
		float result = (num_one + num_two + num_three + num_four + num_five) / 5;
		return result;
	}
	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter five integers: ");
		int num_one = userInput.nextInt();
		int num_two = userInput.nextInt();
		int num_three = userInput.nextInt();
		int num_four = userInput.nextInt();
		int num_five = userInput.nextInt();
		userInput.close();
		PrintAverage object_one = new PrintAverage();
		float avg = object_one.average(num_one, num_two, num_three, num_four, num_five);
		System.out.println("The average of the five numbers is "+avg);
	}
}
