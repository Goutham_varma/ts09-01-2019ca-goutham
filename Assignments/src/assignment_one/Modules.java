//Problem number 5

// A Java program to calculate the modules of two numbers


package assignment_one;
import java.util.Scanner;
public class Modules {
	int first_num;
	int second_num;
	int mod;
	public Modules(int first_num, int second_num) {
		this.first_num = first_num;
		this.second_num = second_num;
	}
	int modulo() {
		mod = first_num % second_num;
		return mod;
	}
	public static void main(String args[]) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter two integers: ");
		int first_num = userInput.nextInt();
		int second_num = userInput.nextInt();
		userInput.close();
		Modules object_one = new Modules(first_num, second_num);
		int mod_result = object_one.modulo();
		System.out.println("The module of the two entered numbers is "+mod_result);
	}
}
