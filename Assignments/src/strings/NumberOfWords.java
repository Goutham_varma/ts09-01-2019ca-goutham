package strings;
import java.util.*;
public class NumberOfWords {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a string: ");
		String input = userInput.nextLine();
		userInput.close();
		int count=1;
		for(int i = 0;i<input.length();i++) {
			if(input.charAt(i) == (char)32) {
				count = count+1;
			}
		}
		System.out.println("The number of words in the sentence are "+count);
	}

}
