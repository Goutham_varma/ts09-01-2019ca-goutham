//counts the number of special characters, upper case letters and lower case letters in the input string
package strings;
import java.util.*;
public class NumberOfSpecialCharLowerUpper {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a string: ");
		String input = userInput.nextLine();
		userInput.close();
		int count_special = 0;
		int count_lower = 0;
		int count_upper = 0;
		for(int i =0;i<input.length();i++) {
			if(65<=(int)input.charAt(i)&&(int)input.charAt(i)<=90) {
				count_upper +=1;
			}
			else if(97<=(int)input.charAt(i)&&(int)input.charAt(i)<=122) {
				count_lower +=1;
			}
			else {
				count_special+=1;
			}
		}
		System.out.println("The number of special charcters in string are "+count_special);
		System.out.println("The number of lower case charcters in string are "+count_lower);
		System.out.println("The number of upper case characters in string are "+count_upper);

	}

}
