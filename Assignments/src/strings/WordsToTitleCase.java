package strings;
import java.util.*;
public class WordsToTitleCase {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a string: ");
		String input = userInput.nextLine();
		userInput.close();
		char[] arr = input.toCharArray();
		StringBuilder output = new StringBuilder();
		for(int i = 0;i<arr.length;i++) {
			if(arr[i] == (char)32 || i==0) {
				if(i==0) {
					arr[0] = (char)(((int)arr[0])-32);
				}
				if(97<=(int)arr[i+1] && (int)arr[i+1]<=122) {
					arr[i+1] = (char)(((int)arr[i+1])-32);
				}
			}
		}
		for(int i=0;i<arr.length;i++) {
			output.append(arr[i]);
		}
		System.out.println(output);
	}

}
