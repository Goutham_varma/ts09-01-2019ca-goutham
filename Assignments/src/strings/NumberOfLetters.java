//counts the number of letters in the string
package strings;
import java.util.*;
public class NumberOfLetters {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a string: ");
		String input = userInput.nextLine();
		userInput.close();
		int count = 0;
		for(int i = 0;i<input.length();i++) {
			if((65<=(int)input.charAt(i)&&(int)input.charAt(i)<=90)||(97<=(int)input.charAt(i)&&(int)input.charAt(i)<=122)) {
				count = count+1;
			}
		}
		System.out.println("The number of letters in the string are "+count);
	}

}
