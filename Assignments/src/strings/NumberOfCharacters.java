//counts the number of characters in the input string
package strings;
import java.util.*;
public class NumberOfCharacters {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a string: ");
		String input = userInput.nextLine();
		userInput.close();
		int count=0;
		char[] arr = input.toCharArray();
		for(int i = 0;i<arr.length;i++) {
			if(0<=(int)arr[i]&&(int)arr[i]<=255) {
				count = count+1;
			}
		}
		System.out.println("The number of characters in the sentence are "+count);
	}

}
