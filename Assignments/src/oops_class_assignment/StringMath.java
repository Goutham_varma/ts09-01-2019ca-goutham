package oops_class_assignment;

class StringMath implements Math {
	String first_str;
	String second_str;
	public StringMath(String first_str, String second_str) {
		this.first_str = first_str;
		this.second_str = second_str;
	}
	public void addition() {
		String result = first_str + second_str;
		System.out.println("The string after addition is: "+ result);
	}
	public void subtraction() {
		System.out.println("String subtraction is not possible");
	}
	public void multiply() {
		System.out.println("String multiplication is not possible");
	}
	public void divison() {
		System.out.println("String divison is not possible");
	}
}

