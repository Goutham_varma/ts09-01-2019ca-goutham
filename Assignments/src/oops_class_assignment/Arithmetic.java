package oops_class_assignment;

public class Arithmetic implements Math {
	int first_num;
	int second_num;
	static int result;
	public Arithmetic(int first_num, int second_num) {
		this.first_num = first_num;
		this.second_num = second_num;
	}
	public void addition() {
		result = first_num + second_num;
		System.out.println("The result after adding is "+result);
	}
	public void subtraction() {
		result = first_num - second_num;
		System.out.println("The result after subtraction is "+result);
	}
	public void multiply() {
		result = first_num * second_num;
		System.out.println("The result after subtraction is "+result);
	}
	public void divison() {
		float result = (float)first_num / (float) second_num;
		System.out.println("The result after subtraction is "+result);
	}
}

class AddThree extends Arithmetic{
	int third_num;
	public AddThree(int first_num, int second_num, int third_num) {
		super(first_num,second_num);
		this.third_num = third_num;
	}
	public void addition() {
		result = first_num + second_num+third_num;
		System.out.println("The result after adding is "+result);
	}	
}

