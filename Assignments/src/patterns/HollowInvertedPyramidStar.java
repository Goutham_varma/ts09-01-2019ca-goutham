package patterns;

public class HollowInvertedPyramidStar {

	public static void main(String[] args) {
		for(int i=0;i<=4;i++) {
			for(int j=0;j<i;j++) {
				System.out.print(" ");
			}
			if(i==1) {
				System.out.print("*     *");
			}
			else if(i==2) {
				System.out.print("*   *");
			}
			else if(i==3) {
				System.out.print("* *");
			}
			else {
				for(int k=9;k>=(2*i+1);k--) {
					System.out.print("*");
				}
			}
			System.out.println("");
		}


	}

}
