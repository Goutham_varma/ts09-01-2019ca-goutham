package patterns;

public class EightStar {

	public static void main(String[] args) {
		for(int i =0;i<9;i++) {
			if(i==0 || i==4 || i==8) {
				System.out.print(" *** ");
			}
			else {
				System.out.print("*   *");
			}
			System.out.println("");
		}

	}

}
