package patterns;

public class HollowMirroredRhombusStar {

	public static void main(String[] args) {
		for(int i = 0;i<5;i++) {
			for(int k = 0;k<i;k++) {
				System.out.print(" ");
			}
			if(i==0 || i==4) {
				for(int j = 0;j<5;j++) {
					System.out.print("*");
				}
			}
			else {
				System.out.print("*   *");
			}
			System.out.println("");
		}

	}

}
