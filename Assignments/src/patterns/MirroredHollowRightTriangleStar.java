package patterns;

public class MirroredHollowRightTriangleStar {
	public static void main(String[] args) {
		for(int i=0;i<5;i++) {
			for(int j = 4;j>i;j--) {
				System.out.print(" ");
			}
			if(i==2) {
				System.out.print("* *");
			}
			else if(i==3) {
				System.out.print("*  *");
			}
			else {
				for(int j=0;j<=i;j++) {
					System.out.print("*");
				}
			}
			System.out.println("");
		}
	}
}
