package patterns;

public class HollowPyramidStar {

	public static void main(String[] args) {
		for(int i=0;i<=4;i++) {
			for(int j=4;j>i;j--) {
				System.out.print(" ");
			}
			if(i==0 || i==4) {
				for(int k=1;k<=(2*i+1);k++) {
					System.out.print("*");
				}
			}
			else if(i==1) {
				System.out.print("* *");
			}
			else if(i==2) {
				System.out.print("*   *");
			}
			else {
				System.out.print("*     *");
			}
			System.out.println("");
		}

	}

}
