package patterns;

public class HollowSquaresStarPattern {

	public static void main(String[] args) {
		for(int i = 0;i<5;i++) {
			if(i==0 || i==4) {
				for(int j = 0;j<5;j++) {
					System.out.print("*");
				}
			}
			else {
				System.out.print("*   *");
			}
			System.out.println("");
		}

	}

}
