package patterns;

public class HollowInvertedRightTriangle {

	public static void main(String[] args) {
		for(int i=0;i<5;i++) {
			if(i==1) {
				System.out.print("*  *");
			}
			else if(i==2) {
				System.out.print("* *");
			}
			else {
				for(int j=5;j>i;j--) {
					System.out.print("*");
				}
			}
			
			System.out.println();
		}

	}

}
