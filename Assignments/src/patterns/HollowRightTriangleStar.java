package patterns;

public class HollowRightTriangleStar {
	public static void main(String[] args) {
		for(int i=0;i<5;i++) {
			if(i==2) {
				System.out.print("* *");
			}
			else if(i==3) {
				System.out.print("*  *");
			}
			else {
				for(int j=0;j<=i;j++) {
					System.out.print("*");
				}
			}
			System.out.println("");
		}
	}
}
