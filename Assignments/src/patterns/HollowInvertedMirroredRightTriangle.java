package patterns;

public class HollowInvertedMirroredRightTriangle {

	public static void main(String[] args) {
		for(int i = 0;i<5;i++) {
			for(int k = 0;k<i;k++) {
				System.out.print(" ");
			}
			if(i==1) {
				System.out.print("*  *");
			}
			else if(i==2) {
				System.out.print("* *");
			}
			else {
				for(int j=5;j>i;j--) {
					System.out.print("*");
				}
			}
			System.out.println("");
		}


	}

}
