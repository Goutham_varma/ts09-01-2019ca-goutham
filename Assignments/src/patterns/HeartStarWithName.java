package patterns;

public class HeartStarWithName {

	public static void main(String[] args) {
		System.out.println("  *****     *****  ");
		System.out.println(" *******   ******* ");
		System.out.println("********* *********");
		for(int i=0;i<=9;i++) {
			for(int j=0;j<i;j++) {
				System.out.print(" ");
			}
			if(i==0) {
				System.out.print("**TALENTSCULPTORS**");
			}
			else {
				for(int k=19;k>=(2*i+1);k--) {
					System.out.print("*");
				}
			}
			System.out.println("");
		}

	}

}
