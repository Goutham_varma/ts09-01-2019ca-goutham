package patterns;

public class HollowDiamondStar {

	public static void main(String[] args) {
		for(int i = 0;i<10;i++) {
			if(i==0 || i==9) {
				for(int j=0;j<10;j++) {
					System.out.print("*");
				}
			}
			else if(i==1 || i ==8) {
				System.out.print("****  ****");
			}
			else if(i==2 || i==7) {
				System.out.print("***    ***");
			}
			else if(i==3 || i==6) {
				System.out.print("**      **");
			}
			else if(i==4 || i==5) {
				System.out.print("*        *");
			}
			System.out.println("");
		}

	}

}
