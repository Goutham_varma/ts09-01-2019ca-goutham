package patterns;

public class HeartStar {

	public static void main(String[] args) {
			System.out.println("  *****     *****  ");
			System.out.println(" *******   ******* ");
			System.out.println("********* *********");
			for(int i=0;i<=9;i++) {
				for(int j=0;j<i;j++) {
					System.out.print(" ");
				}
				for(int k=19;k>=(2*i+1);k--) {
					System.out.print("*");
				}
				System.out.println("");
			}
		}

	}


